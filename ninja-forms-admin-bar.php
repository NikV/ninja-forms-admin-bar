<?php
/**
 * Plugin Name: Ninja Forms - Admin Bar
 * Description: A toolbar extension for Ninja Forms which allows for quick navigation to different parts of Ninja Forms and a way to see form actions and markup.
 * Author: Nikhil Vimal
 * Author URI: http://nik.techvoltz.com
 * Version: 1.0
 * Plugin URI:
 * License: GNU GPLv2+
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

Class Ninja_Forms_Admin_Bar {

	//The Ninja Forms Admin Bar Instance
	public static function init() {
		static $instance = false;
		if ( ! $instance ) {
			$instance = new Ninja_Forms_Admin_Bar();
		}
		
		return $instance;
	}

	//Lets get this party started
	public function __construct() {
		//Only load if outside of the admin area
		if ( !is_admin() ) {
			add_action( 'admin_bar_menu', array( $this, 'ninja_forms_admin_nodes' ), 999 );
		}
		//We need to enqueue some scripts from the /css folder
		add_action('wp_enqueue_scripts', array ( $this, 'ninja_forms_hooks_stylesheet'));
		add_action('get_header', array( $this, 'ninja_forms_hooks' ));



	}


	/**
	 * Registers Nodes in the admin bar
	 *
	 * @param $wp_admin_bar The WordPress admin bar
	 */
	function ninja_forms_admin_nodes( $wp_admin_bar ) {

		$wp_admin_bar->add_node( array(
				'parent' => 'ninja_form_admin_bar',
				'id'     => 'ninja_forms_new',
				'title'  => 'New Form',
				'href'   => admin_url('admin.php?page=ninja-forms&form_id=new&tab=form_settings'),
			)
		);

		$wp_admin_bar->add_node( array(
				'parent' => 'ninja_form_admin_bar',
				'id'     => 'ninja_forms_submissions',
				'title'  => 'View All Submissions',
				'href'   => admin_url('admin.php?page=ninja-forms&form_id=new&tab=form_settings'),
			)
		);

		$wp_admin_bar->add_node( array(
				'parent' => 'new-content',
				'id'     => 'ninja_form_new',
				'title'  => 'Form',
				'href'   => admin_url( 'admin.php?page=ninja-forms&tab=form_settings&form_id=new' ),
			)
		);

		$wp_admin_bar->add_node( array(
				'id'     => 'ninja_form_admin_bar',
				'title'  => 'Ninja Forms',
				'href'   => admin_url( 'admin.php?page=ninja-forms&tab=form_settings&form_id=new' ),
			)
		);

		$wp_admin_bar->add_node( array(
				'parent' => 'ninja_form_admin_bar',
				'id'     => 'ninja_forms_hook_guide',
				'title'  => 'Ninja Forms Hooks',
				'href'   => add_query_arg( 'ninja_forms_hooks', 'display' ),
			)
		);


		$wp_admin_bar->add_node( array(
				'parent' => 'ninja_form_admin_bar',
				'id'     => 'ninja_forms_markup',
				'title'  => 'Ninja Forms Markup',
				'href'   => add_query_arg( 'ninja_forms_markup', 'display' ),
			)
		);

		$wp_admin_bar->add_node( array(
				'parent' => 'ninja_form_admin_bar',
				'id'     => 'ninja_forms_clear',
				'title'  => 'Clear Hooks and Markup',
				'href'   => remove_query_arg( array('ninja_forms_markup', 'ninja_forms_hooks') ),
			)
		);

		//This one appears under the WordPress logo in the admin bar
		$wp_admin_bar->add_node( array(
				'parent' => 'wp-logo',
				'id'     => 'ninja_form_under_wp_logo',
				'title'  => 'About Ninja Forms',
				'href'   => 'http://ninjaforms.com/features/',
			)
		);
	}

	/**
	 * Enqueue stylesheets if the query_arg exsists
	 *
	 * @since 1.0
	 */
	public function ninja_forms_hooks_stylesheet() {

		$nf_admin_bar_url = plugin_dir_url( __FILE__ );

		if ( 'display' == isset( $_GET['ninja_forms_hooks'] ) ) {
			wp_enqueue_style( 'nf_hook_styles', $nf_admin_bar_url . '/assets/css/styles.css' );
		}


		if ( 'display' == isset( $_GET['ninja_forms_markup'] ) ) {
			wp_enqueue_style( 'nf_markup_styles', $nf_admin_bar_url . '/assets/css/markup.css' );
		}

	}


	/**
	 * Collects all of the hooks and gives them a small popup when you hover over
	 *
	 * @since 1.0
	 */
	public function ninja_forms_hooks() {
		global $ninja_forms_hooks;

		if ( ! ( 'display' == isset( $_GET['ninja_forms_hooks'] ) ) && ! ( 'display' == isset( $_GET['ninja_forms_markup'] ) ) ) {
			return;  //Don't display anything if the query is not added

		}

		$ninja_forms_hooks = array(

			'ninja_forms_display_css' => array(
				'hook' => 'ninja_forms_display_css',
				'area' => 'Structural',
				'description' => 'This is the last hook run when displaying any Ninja Form. This is used for enqueueing general and form specific css files.',
				'functions' => array(),
			),
			'ninja_forms_before_form_display' => array(
				'hook' => 'ninja_forms_before_form_display',
				'area' => 'Structural',
				'description' => 'This hook is run before any form output is begun.',
				'functions' => array(),
			),
			'ninja_forms_display_before_form_title' => array(
				'hook' => 'ninja_forms_display_before_form_title',
				'area' => 'Structural',
				'description' => 'This hook executes immediately after the header (outside the #header div).',
				'functions' => array(),
			),

			'ninja_forms_display_before_field' => array(
				'hook' => 'ninja_forms_display_before_field',
				'area' => 'Structural',
				'description' => 'This hook is run for every field in the form. It is passed the $field_id and $data variables. The $data variable is an array that contains all relevant field settings.

It is ran before each field is rendered.',
				'functions' => array(),
			),
			'ninja_forms_display_after_field' => array(
				'hook' => 'ninja_forms_display_after_field',
				'area' => 'Structural',
				'description' => 'This hook is run for every field in the form. It is passed the $field_id and $data variables. The $data variable is an array that contains all relevant field settings.

It runs after all field rendering is complete.',
				'functions' => array(),
			),
			'ninja_forms_display_after_form' => array(
				'hook' => 'ninja_forms_display_after_form',
				'area' => 'Structural',
				'description' => 'This hook is run immediately after the closing form tag.',
				'functions' => array(),
			),


		);

		foreach ( $ninja_forms_hooks as $action ) {
			add_action( $action['hook'], array( $this, 'ninja_forms_actions' ), 1 );
		}
	}

	/**
	 * Dispaly the actions
	 *
	 * @since 1.0
	 */
	public function ninja_forms_actions() {
		global $ninja_forms_hooks;

		$current_action = current_filter();

		if ( 'display' == isset( $_GET['ninja_forms_hooks'] ) ) {

			if ( 'Document Head' == $ninja_forms_hooks[$current_action]['area'] ) {

				echo "<!-- ";
				echo $current_action;
				echo " -->\n";
			}

			else {

				echo '<div class="nf_hook" title="' . $ninja_forms_hooks[ $current_action ]['description'] . '">' . $current_action . '</div>';
			}

		}

	}
}

function load_nf_admin_bar() {

	if( !function_exists( 'is_plugin_active' ) ) {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	}

	//Check if plugin is activated, or else...
	if( is_plugin_active( 'ninja-forms/ninja-forms.php' ) ) {

		return Ninja_Forms_Admin_Bar::init();

	}

}
add_action('plugins_loaded', 'load_nf_admin_bar');